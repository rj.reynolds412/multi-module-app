package com.example.feature_shibe.model

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {


    companion object {
        private const val BASE_URL = "https://shibe.online"
        private const val QUERY_CATEGORY = "count"

        fun getInstance(): ShibeService {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create()
        }
    }

    @GET("/api/shibes")
    suspend fun getShibes(@Query(QUERY_CATEGORY) count: Int = 100): List<String>

}