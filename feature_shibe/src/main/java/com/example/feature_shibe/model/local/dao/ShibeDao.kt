package com.example.feature_shibe.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import com.example.feature_shibe.model.local.entity.Shibe

@Dao
interface ShibeDao {
    @androidx.room.Query("SELECT * FROM shibe")
    suspend fun getAll(): List<Shibe>


    @Insert
    suspend fun insert(shibe: List<Shibe>)
}
