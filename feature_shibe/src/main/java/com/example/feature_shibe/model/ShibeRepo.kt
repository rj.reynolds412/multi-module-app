package com.example.feature_shibe.model

import android.content.Context
import com.example.feature_shibe.model.local.dao.ShibeDatabase
import com.example.feature_shibe.model.local.entity.Shibe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ShibeRepo(context: Context) {

    private val shibeService by lazy { ShibeService.getInstance() }
    private val shibeDao = ShibeDatabase.getInstance(context).shibeDao()


    suspend fun getShibes() = withContext(Dispatchers.IO) {
        val savedShibes: List<Shibe> = shibeDao.getAll()

        return@withContext savedShibes.ifEmpty {
            val shibeUrls: List<String> = shibeService.getShibes()
            val shibes: List<Shibe> = shibeUrls.map { Shibe(url = it) }
            shibeDao.insert(shibes)
            return@ifEmpty shibes
        }
    }

}