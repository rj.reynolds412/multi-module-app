package com.example.feature_shibe.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.feature_shibe.databinding.ItemShibeBinding
import com.example.feature_shibe.model.local.entity.Shibe

class ShibeAdapter(
    private val shibeImages: List<Shibe>,
) : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ShibeViewHolder.newInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val shibeImages = shibeImages[position]
        holder.getShibes(shibeImages)
    }


    override fun getItemCount(): Int = shibeImages.size


    class ShibeViewHolder(
        val binding: ItemShibeBinding,
    ) : RecyclerView.ViewHolder(binding.root) {

        fun getShibes(shibeImages: Shibe) {
            Log.d("ShibeAdapter", "my image url is ${shibeImages.url} ")
            binding.ivShibe.load(shibeImages.url)
        }

        companion object {
            fun newInstance(parent: ViewGroup) = ItemShibeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { ShibeViewHolder(it) }
        }
    }
}
