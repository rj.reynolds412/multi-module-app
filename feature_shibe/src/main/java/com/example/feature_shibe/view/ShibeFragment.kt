package com.example.feature_shibe.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.feature_shibe.adapter.ShibeAdapter
import com.example.feature_shibe.databinding.FragmentShibeBinding
import com.example.feature_shibe.model.ShibeRepo
import com.example.feature_shibe.viewModel.ShibeViewModel

class ShibeFragment : Fragment() {
    private var _binding: FragmentShibeBinding? = null
    private val binding get() = _binding!!
    private val shibeViewModel by viewModels<ShibeViewModel>() {
        ShibeViewModel.ShibeViewModelFactory(ShibeRepo(requireContext()))
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentShibeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shibeViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.run {
                loading.isVisible = state.isLoading
                rvShibe.adapter = ShibeAdapter(state.shibes)
            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}