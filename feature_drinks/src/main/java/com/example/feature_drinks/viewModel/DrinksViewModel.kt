package com.example.feature_drinks.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_drinks.model.BottomsUpRepo
import com.example.feature_drinks.view.state.DrinkListState
import kotlinx.coroutines.launch

class DrinksViewModel : ViewModel() {

    private val repo by lazy { BottomsUpRepo }
    private val _drinkState = MutableLiveData(DrinkListState(isLoading = true))
    val drinkState: LiveData<DrinkListState> get() = _drinkState


    fun getDrinkCategory(category: String) {
        viewModelScope.launch {
            val categoryDrinksDTO = repo.getCategoryDrinks(category)
            _drinkState.value = DrinkListState(drinkslist = categoryDrinksDTO.drinks)
        }
    }
}