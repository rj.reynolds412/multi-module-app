package com.example.feature_drinks.view.state

import com.example.feature_drinks.model.response.CategoryDrinksDTO

data class DrinkListState(
val isLoading: Boolean = false,
val drinkslist: List<CategoryDrinksDTO.Drink> = emptyList()
)
