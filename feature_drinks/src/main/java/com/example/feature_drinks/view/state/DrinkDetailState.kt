package com.example.feature_drinks.view.state
import com.example.feature_drinks.model.response.DrinkDetailsDTO

data class DrinkDetailState(
    val isLoading: Boolean = false,
    val drinkDetail: List<DrinkDetailsDTO.Drink> = emptyList()
)
