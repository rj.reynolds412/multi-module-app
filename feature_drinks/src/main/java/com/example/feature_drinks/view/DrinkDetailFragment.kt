package com.example.feature_drinks.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.feature_drinks.databinding.FragmentDrinkDetailBinding
import com.example.feature_drinks.model.response.DrinkDetailsDTO
import com.example.feature_drinks.viewModel.DrinkDetailViewModel

class DrinkDetailFragment : Fragment() {
    private var _binding: FragmentDrinkDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DrinkDetailFragmentArgs>()
    private val drinksDetailViewModel by viewModels<DrinkDetailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentDrinkDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val drinkDetails = args.drinkId.toInt()
        drinksDetailViewModel.getDrinkDetails(drinkDetails)
        drinksDetailViewModel.detailState.observe(viewLifecycleOwner) {
            if (it.isLoading) {
                binding.loading.isVisible
            } else {
                binding.loading.isVisible = false
                displayDrink(it.drinkDetail.first())

            }
        }
    }

    private fun displayDrink(drink: DrinkDetailsDTO.Drink) {
        binding.ivDrink2.load(drink.strDrinkThumb)
        binding.tvDrink2.text = drink.strDrink
        binding.tvDetails.text = drink.strInstructions
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}