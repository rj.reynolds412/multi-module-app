package com.example.feature_drinks.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.feature_drinks.databinding.FragmentDrinksBinding
import com.example.feature_drinks.model.remote.adapter.DrinksAdapter
import com.example.feature_drinks.model.response.CategoryDrinksDTO
import com.example.feature_drinks.viewModel.DrinksViewModel

class DrinksFragment : Fragment() {
    private var _binding: FragmentDrinksBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DrinksFragmentArgs>()
    private val drinksViewModel by viewModels<DrinksViewModel>()
    private val drinksAdapter by lazy { DrinksAdapter(::drinkClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentDrinksBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val drinkCategory = args.category
        drinksViewModel.getDrinkCategory(drinkCategory)
        drinksViewModel.drinkState.observe(viewLifecycleOwner) { state ->
            drinksAdapter.loadDrinks(state.drinkslist)
            binding.rvDrinks.adapter = drinksAdapter
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun drinkClicked(drink: CategoryDrinksDTO.Drink) {
        val drinkNav =
            DrinksFragmentDirections.actionDrinksFragmentToDrinkDetailFragment(drink.idDrink)
        findNavController().navigate(drinkNav)
    }
}