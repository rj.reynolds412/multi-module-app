package com.example.feature_health.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.feature_health.databinding.FragmentHealthyPlanBinding
import com.example.feature_health.viewModel.SleepViewModel

class HealthyPlanFragment : Fragment() {
    private var _binding: FragmentHealthyPlanBinding? = null
    private val binding get() = _binding!!
    private val sleepViewModel by viewModels<SleepViewModel>()
    private var selectedIndex = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentHealthyPlanBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            btnContinue.isEnabled = selectedIndex != 0
            rg1.setOnCheckedChangeListener { radioGroup: RadioGroup?, i: Int ->
                selectedIndex = i
                btnContinue.isEnabled = selectedIndex != 0
            }
            btnContinue.setOnClickListener {
                sleepViewModel.getSleepHours(selectedIndex)
                progressCircular.visibility = View.VISIBLE
            }
        }

        sleepViewModel.sleep.observe(viewLifecycleOwner) { sleepString ->
            val finalDirection =
                HealthyPlanFragmentDirections.actionHealthyPlanFragmentToFinalFragment(sleepString)
            findNavController().navigate(finalDirection)
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}