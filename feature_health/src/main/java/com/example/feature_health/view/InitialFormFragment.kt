package com.example.feature_health.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.feature_health.databinding.FragmentInitialFormBinding

class InitialFormFragment : Fragment() {
    private var _binding: FragmentInitialFormBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentInitialFormBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnCreatePlan.setOnClickListener {
            val initnav =
                InitialFormFragmentDirections.actionInitialFormFragmentToHealthyPlanFragment()
            findNavController().navigate(initnav)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}