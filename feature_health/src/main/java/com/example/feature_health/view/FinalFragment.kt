package com.example.feature_health.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.feature_health.databinding.FragmentFinalBinding

class FinalFragment : Fragment() {
    private var _binding: FragmentFinalBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<FinalFragmentArgs>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentFinalBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sleepmessage = args.message
        binding.tvFinal.text = sleepmessage
        binding.btnClose.setOnClickListener {
            activity?.finish()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}