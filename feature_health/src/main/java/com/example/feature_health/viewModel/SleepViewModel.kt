package com.example.feature_health.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_health.model.SleepRepo
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SleepViewModel : ViewModel() {
    private val sleepRepo = SleepRepo

    private val _sleep = MutableLiveData<String>()
    val sleep: LiveData<String> get() = _sleep

    fun getSleepHours(index: Int) {
        viewModelScope.launch {
            delay(when (index) {
                1 -> 6000L
                2 -> 8000L
                3 -> 9000L
                else -> 0L
            })
            val sleepHours = sleepRepo.getSleepHours(index)
            _sleep.value = sleepHours
        }
    }
}