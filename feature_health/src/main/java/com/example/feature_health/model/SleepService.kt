package com.example.feature_health.model

interface SleepService {
    suspend fun getSleepHours(index: Int): String
}