package com.example.feature_health.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object SleepRepo {

    private val sleepService = object : SleepService {
        override suspend fun getSleepHours(index: Int): String {
            val list = listOf(
                "You ain't even sleep 6 hours. Hit snooze!",
                "Nice!! You got 8 of the thangs!! ",
                "More than 9?! C'mon bro GET UP!!",
            )
            return list[index - 1]
        }
    }

    suspend fun getSleepHours(index: Int) = withContext(Dispatchers.IO) {
        sleepService.getSleepHours(index)
    }
}